import { sensorConstants } from '../_constants';
import { sensorService } from '../_services';

export const sensorActions = { getData };

function getData() {
    return dispatch => {
        dispatch(request());

        sensorService.getData()
            .then(
                data => dispatch(success(data)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: sensorConstants.GET_DATA_REQUEST } }
    function success(data) { return { type: sensorConstants.GET_DATA_SUCCESS, data } }
    function failure(error) { return { type: sensorConstants.GET_DATA_FAILURE, error } }
}