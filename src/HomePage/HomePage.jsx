import React from 'react';
import { connect } from 'react-redux';
import { sensorActions, userActions } from '../_actions';
import { MenuBar, SensorData, DiseaseWarning} from '../_components';

class HomePage extends React.Component {
    componentDidMount() {
        this.props.dispatch(sensorActions.getData());
    }

    render() {
        const { user, sensorData } = this.props;
        const diseasesPL = {
            arthralgia: "Bóle stawów",
            hypertension: "Nadciśnienie",
            albinism: "Albinizm"
        };
        return (
            <div>
                <MenuBar username={user.firstName}/>
                <div className="col-md-3">
                    <div className="row">
                        <h3>Twoje choroby:</h3>
                        <ul>
                            {user.diseases.map(disease =>
                                <li>{diseasesPL[disease]}</li>
                            )}
                        </ul>
                    </div>
                    <div className="row">
                        <button className="btn btn-info weather-btn">
                            <a target="_blank" href={'http://pogoda.' + user.weatherWidget + '.pl'}>
                                SPRAWDŹ POGODĘ W {user.weatherWidget}
                            </a>
                        </button>
                    </div>
                </div>
                <div className="col-md-6">
                    <SensorData {...sensorData} />
                </div>
                <div className="col-md-3">
                    <DiseaseWarning diseases={user.diseases} {...sensorData} />
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { user } = state.authentication;
    return {
        user,
        sensorData: state.sensor.data
    };
}

const connectedHomePage = connect(mapStateToProps)(HomePage);
export { connectedHomePage as HomePage };