import React from 'react';
import { connect } from 'react-redux';
import { MenuBar } from '../_components';

class ContactPage extends React.Component {
    render() {
        const { user } = this.props;
        return (
            <div>
                <MenuBar username={user && user.firstName}/>
                <div className="col-md-10 col-md-offset-2">
                    <h1>Kontakt</h1>
                    <p>Telefon: 123 456 789</p>
                    <p>E-mail: a@a.pl</p>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { user } = state.authentication;
    return { user };
}

const connectedContactPage = connect(mapStateToProps)(ContactPage);
export { connectedContactPage as ContactPage };