import React from 'react';
import { connect } from 'react-redux';
import {Link, Route, Switch} from 'react-router-dom';
import { MenuBar } from '../_components';
import * as diseases from './diseases';

class DiseasesPage extends React.Component {
    render() {
        const { user } = this.props;
        return (
            <div>
                <MenuBar username={user && user.firstName}/>
                <div className="row">
                    <div className="col-md-3">
                        <h3>Spis chorób</h3>
                        <ul>
                            <li><Link to="/diseases/albinism">Albinizm</Link></li>
                            <li><Link to="/diseases/hypertension">Nadciśnienie</Link></li>
                            <li><Link to="/diseases">Coś tam</Link></li>
                        </ul>
                    </div>
                    <div className="col-md-9">
                        <Switch>
                            <Route path="/diseases/albinism" component={diseases.AlbinismPage}/>
                            <Route path="/diseases/hypertension" component={diseases.HypertensionPage}/>
                        </Switch>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { user } = state.authentication;
    return { user };
}

const connectedDiseasesPage = connect(mapStateToProps)(DiseasesPage);
export { connectedDiseasesPage as DiseasesPage };