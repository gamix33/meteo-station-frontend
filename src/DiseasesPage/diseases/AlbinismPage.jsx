import React, { Fragment } from 'react';

export const AlbinismPage = props => (
    <Fragment>
        <h1>Albinizm</h1>
        <p>Jeśli masz albinizm to wyglądasz tak:</p>
        <br />
        <img src="https://upload.wikimedia.org/wikipedia/commons/9/9a/Albinisitic_man_portrait.jpg" />
        <p>więc lepiej chodź w czapce gdy świeci słońce</p>
    </Fragment>
);