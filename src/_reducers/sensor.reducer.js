import { sensorConstants } from '../_constants';

export function sensor(state = {}, action) {
  switch (action.type) {
    case sensorConstants.GET_DATA_REQUEST:
      return {
        loading: true
      };
    case sensorConstants.GET_DATA_SUCCESS:
      return {
        data: action.data
      };
    case sensorConstants.GET_DATA_FAILURE:
      return { 
        error: action.error
      };
    default:
      return state
  }
}