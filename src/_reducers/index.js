import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import { registration } from './registration.reducer';
import { alert } from './alert.reducer';
import { sensor } from './sensor.reducer';

const rootReducer = combineReducers({
  authentication,
  registration,
  alert,
  sensor
});

export default rootReducer;