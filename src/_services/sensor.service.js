import config from 'config';

export const sensorService = { getData };

function getData() {
    return fetch(`${config.apiUrl}/sensor/data`).then(handleResponse);
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }
        return data;
    });
}