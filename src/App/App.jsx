import React, {Fragment} from 'react';
import {Router, Route, Switch} from 'react-router-dom';
import { connect } from 'react-redux';
import { history } from '../_helpers';
import { alertActions } from '../_actions';
import { PrivateRoute } from '../_components';
import { HomePage } from '../HomePage';
import { LoginPage } from '../LoginPage';
import { RegisterPage } from '../RegisterPage';
import { ContactPage } from '../ContactPage';
import { HelpPage } from '../HelpPage';
import { DiseasesPage } from '../DiseasesPage';

import './app.css';

class App extends React.Component {
    constructor(props) {
        super(props);

        const { dispatch } = this.props;
        history.listen((location, action) => {
            dispatch(alertActions.clear());
        });
    }

    render() {
        const { alert, user } = this.props;
        return (
            <Fragment>
                <div className="app">
                    <div className="container">
                        <div className="col-sm-10 col-sm-offset-1">
                            {alert.message &&
                                <div className={`alert ${alert.type}`}>{alert.message}</div>
                            }
                            <Router history={history}>
                                <div>
                                    <PrivateRoute exact path="/" component={HomePage} />
                                    <Route path="/login" component={LoginPage} />
                                    <Route path="/register" component={RegisterPage} />
                                    <Route path="/diseases" component={DiseasesPage} />
                                    <Route path="/contact" component={ContactPage} />
                                    <Route path="/help" component={HelpPage} />
                                </div>
                            </Router>
                        </div>
                    </div>
                </div>
                <div className="text-center">

                </div>
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    const { alert } = state;
    return {
        alert
    };
}

const connectedApp = connect(mapStateToProps)(App);
export { connectedApp as App }; 