import React, {Fragment} from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import {MenuBar, Spinner} from '../_components';

class RegisterPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user: {
                firstName: '',
                lastName: '',
                username: '',
                password: '',
                email: '',
                phone: '',
                notification: 'NONE',
                enabledNotifications: [],
                weatherWidget: 'INTERIA',
                diseases: [],
            },
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const { name, value } = event.target;
        const { user } = this.state;
        const prevValue = user[name];
        const isArray = Array.isArray(prevValue);
        this.setState({
            user: {
                ...user,
                [name]: !isArray
                    ? value
                    : event.target.checked
                        ? prevValue.concat(value)
                        : prevValue.filter(v => v !== value)
            }
        });
    }

    handleSubmit(event) {
        event.preventDefault();

        this.setState({ submitted: true });
        const { user } = this.state;
        const { dispatch } = this.props;
        if (user.firstName && user.lastName && user.username && user.password) {
            dispatch(userActions.register(user));
        }
    }

    render() {
        const { registering  } = this.props;
        const { user, submitted } = this.state;
        return (
            <Fragment>
                <MenuBar/>
                <div className="col-md-10 col-md-offset-1">
                    <h2 className="text-center">Rejestracja</h2>
                    <form name="form" onSubmit={this.handleSubmit}>
                        <div className="col-md-6">
                            <div className={'form-group' + (submitted && !user.firstName ? ' has-error' : '')}>
                                <label htmlFor="firstName">Imię</label>
                                <input type="text" className="form-control" name="firstName" value={user.firstName} onChange={this.handleChange} />
                                {submitted && !user.firstName &&
                                <div className="help-block">Imię jest wymagane</div>
                                }
                            </div>
                            <div className={'form-group' + (submitted && !user.lastName ? ' has-error' : '')}>
                                <label htmlFor="lastName">Nazwisko</label>
                                <input type="text" className="form-control" name="lastName" value={user.lastName} onChange={this.handleChange} />
                                {submitted && !user.lastName &&
                                <div className="help-block">Nazwisko jest wymagane</div>
                                }
                            </div>
                            <div className={'form-group' + (submitted && !user.username ? ' has-error' : '')}>
                                <label htmlFor="username">Login</label>
                                <input type="text" className="form-control" name="username" value={user.username} onChange={this.handleChange} />
                                {submitted && !user.username &&
                                <div className="help-block">Login jest wymagany</div>
                                }
                            </div>
                            <div className={'form-group' + (submitted && !user.password ? ' has-error' : '')}>
                                <label htmlFor="password">Hasło</label>
                                <input type="password" className="form-control" name="password" value={user.password} onChange={this.handleChange} />
                                {submitted && !user.password &&
                                <div className="help-block">Hasło jest wymagane</div>
                                }
                            </div>
                            <div className={'form-group' + (submitted && !user.username ? ' has-error' : '')}>
                                <label htmlFor="email">Email</label>
                                <input type="text" className="form-control" name="email" value={user.email} onChange={this.handleChange} />
                                {submitted && !user.email &&
                                <div className="help-block">Email jest wymagany</div>
                                }
                            </div>
                            <div className={'form-group' + (submitted && !user.username ? ' has-error' : '')}>
                                <label htmlFor="phone">Nr telefonu</label>
                                <input type="text" className="form-control" name="phone" value={user.phone} onChange={this.handleChange} />
                                {submitted && !user.phone &&
                                <div className="help-block">Nr telefonu jest wymagany</div>
                                }
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className={'form-group'}>
                                <p>Powiadomienia</p>
                                <div className="row">
                                    <div className="col-md-4">
                                        <input type="radio"
                                               name="notification"
                                               id="notification-none"
                                               value="NONE"
                                               onChange={this.handleChange}
                                               checked={user.notification === 'NONE'} />
                                        <label htmlFor="notification-none">wyłączone</label>
                                    </div>
                                    <div className="col-md-4">
                                        <input type="radio"
                                               name="notification"
                                               id="notification-sms"
                                               value="SMS"
                                               onChange={this.handleChange}
                                               checked={user.notification === 'SMS'} />
                                        <label htmlFor="notification-sms">SMS</label>
                                    </div>
                                    <div className="col-md-4">
                                        <input type="radio"
                                               name="notification"
                                               id="notification-email"
                                               value="EMAIL"
                                               onChange={this.handleChange}
                                               checked={user.notification === 'EMAIL'}/>
                                        <label htmlFor="notification-email">EMAIL</label>
                                    </div>
                                </div>
                            </div>
                            <div className={'form-group'}>
                                <p>Typ powiadomień</p>
                                <input type="checkbox"
                                       name="enabledNotifications"
                                       value="TEMPERATURE"
                                       onChange={this.handleChange}
                                       checked={user.enabledNotifications.includes('TEMPERATURE')}/>
                                temperatura <br />
                                <input type="checkbox"
                                       name="enabledNotifications"
                                       value="HUMIDITY"
                                       onChange={this.handleChange}
                                       checked={user.enabledNotifications.includes('HUMIDITY')}/>
                                wilgotność <br />
                                <input type="checkbox"
                                       name="enabledNotifications"
                                       value="LIGHT"
                                       onChange={this.handleChange}
                                       checked={user.enabledNotifications.includes('LIGHT')}/>
                                nasłonecznienie <br />
                                <input type="checkbox"
                                       name="enabledNotifications"
                                       value="RAIN"
                                       onChange={this.handleChange}
                                       checked={user.enabledNotifications.includes('RAIN')}/>
                                deszcz <br />
                                <input type="checkbox"
                                       name="enabledNotifications"
                                       value="PRESSURE"
                                       onChange={this.handleChange}
                                       checked={user.enabledNotifications.includes('PRESSURE')}/>
                                ciśnienie <br />
                            </div>
                            <div className={'form-group'}>
                                <p>Info o pogodzie</p>
                                <div className="row">
                                    <div className="col-md-4">
                                        <input type="radio"
                                               name="weatherWidget"
                                               id="weatherWidget-interia"
                                               value="INTERIA"
                                               onChange={this.handleChange}
                                               checked={user.weatherWidget === 'INTERIA'} />
                                        <label htmlFor="weatherWidget-interia">INTERIA</label>
                                    </div>
                                    <div className="col-md-4">
                                        <input type="radio"
                                               name="weatherWidget"
                                               id="weatherWidget-onet"
                                               value="ONET"
                                               onChange={this.handleChange}
                                               checked={user.weatherWidget === 'ONET'} />
                                        <label htmlFor="weatherWidget-onet">ONET</label>
                                    </div>
                                </div>
                            </div>
                            <div className={'form-group'}>
                                <p>Twoje choroby</p>
                                <input type="checkbox"
                                       name="diseases"
                                       value="hypertension"
                                       onChange={this.handleChange}
                                       checked={user.diseases.includes('hypertension')}/>
                                nadciśnienie <br />
                                <input type="checkbox"
                                       name="diseases"
                                       value="arthralgia"
                                       onChange={this.handleChange}
                                       checked={user.diseases.includes('arthralgia')}/>
                                bóle stawów <br />
                                <input type="checkbox"
                                       name="diseases"
                                       value="albinism"
                                       onChange={this.handleChange}
                                       checked={user.diseases.includes('albinism')}/>
                                albinizm <br />
                            </div>
                        </div>
                        <div className="form-group text-center">
                            {!registering && <button className="btn btn-primary">Zarejestruj</button>}
                            {!registering && <Link to="/login" className="btn btn-link">Anuluj</Link>}
                            <Spinner visible={registering} />
                        </div>
                    </form>
                </div>
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    const { registering } = state.registration;
    return {
        registering
    };
}

const connectedRegisterPage = connect(mapStateToProps)(RegisterPage);
export { connectedRegisterPage as RegisterPage };