import React from 'react';
import { Link } from 'react-router-dom';

import './menuBar.css';

export const MenuBar = props => {
    const { username } = props;
    return (
        <nav className="navbar navbar-default">
            <div className="container-fluid">
                <div className="navbar-header">
                    <Link className="navbar-brand" to="/">Meteo</Link>
                </div>
                <ul className="nav navbar-nav">
                    <li><Link to="/">Strona główna</Link></li>
                    <li className="dropdown">
                        <a className="dropdown-toggle" data-toggle="dropdown" href="#">
                            Spis chorób
                            <span className="caret"></span>
                        </a>
                        <ul className="dropdown-menu">
                            <li><Link to="/diseases/albinism">Albinizm</Link></li>
                            <li><Link to="/diseases/hypertension">Nadciśnienie</Link></li>
                            <li><Link to="/diseases">Coś tam</Link></li>
                        </ul>
                    </li>
                    <li><Link to="/contact">Kontakt</Link></li>
                    <li><Link to="/help">Pomoc</Link></li>
                </ul>
                {username &&
                    <div className="navbar-login">
                        Zalogowany jako <strong>{username}</strong>!
                        <button className="btn">
                            <Link to="/login">Wyloguj</Link>
                        </button>
                    </div>
                }
            </div>
        </nav>
    );
};