import React, { Fragment } from 'react';
import './sensorData.css';
import * as weatherIcons from '../_assets/weather_icons';

export class SensorData extends React.Component {
  constructor(props) {
      super(props);
  }

  render() {
      const { temperature, rain, humidity, light, pressure } = this.props;
      const rainInfo = {
          NO_RAIN: 'Brak deszczu.',
          SMALL_RAIN: 'Pada niewielki deszcz, jeśli jesteś z cukru rozważ wzięcie ze sobą parasola.',
          MEDIUM_RAIN: 'Występują średnie opady deszczu, weź parasol.',
          STRONG_RAIN: 'Na zewnątrz mocno pada, koniecznie weź parasol i pelerynę.',
      };
      const goodPressure = pressure > 1005 && pressure < 1021;
      return (
          <div className="sensor-data">
              <div className="row">
                  <div className="col-md-8 sensor-label">Temperatura: <b>{temperature} &deg;C</b></div>
                  <div className="col-md-4">
                      {temperature >= 20 && <img src={weatherIcons.highTemp} />}
                      {temperature < 20 && temperature >= 5 && <img src={weatherIcons.mediumTemp} />}
                      {temperature < 5 && <img src={weatherIcons.lowTemp} />}
                  </div>
              </div>
              <div className="row">
                  <div className="col-md-8 sensor-label"><b>{rainInfo[rain]}</b></div>
                  <div className="col-md-4">
                      {rain == 'SMALL_RAIN' && <img src={weatherIcons.smallRain} />}
                      {rain == 'MEDIUM_RAIN' && <img src={weatherIcons.mediumRain} />}
                      {rain == 'STRONG_RAIN' && <img src={weatherIcons.strongRain} />}
                  </div>
              </div>
              <div className="row">
                  <div className="col-md-8 sensor-label">Wilgotność: <b>{humidity}</b></div>
                  <div className="col-md-4">

                  </div>
              </div>
              <div className="row">
                  <div className="col-md-8 sensor-label">
                      {light > 25000 && 'Mocno swieci slonce, koniecznie wez czapke i okulary słoneczne.'}
                      {light > 5000 && light < 25000 && 'Średnio świeci słońce wez ze sobą czapkę i okulary przeciwsłoneczne.'}
                      {light < 5000 && 'Lekko świeci słońce, nie ubieraj czapki ani okularów przeciwsłonecznych.'}
                  </div>
                  <div className="col-md-4">
                      {light > 25000 && <img src={weatherIcons.strongSun}/>}
                      {light > 5000 && light < 25000 && <img src={weatherIcons.mediumSun}/>}
                      {light < 5000 && <img src={weatherIcons.noSun}/>}
                  </div>
              </div>
              <div className="row">
                  <div className="col-md-8 sensor-label">Ciśnienie: <b>{pressure} hPa</b></div>
                  <div className="col-md-4">
                      {goodPressure && <img src={weatherIcons.goodPressure}/>}
                      {!goodPressure && <img src={weatherIcons.badPressure}/>}
                  </div>
              </div>
          </div>
      );
  }
}