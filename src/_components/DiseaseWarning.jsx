import React, { Fragment } from 'react';
import './diseaseWarning.css';

export class DiseaseWarning extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { diseases, temperature, humidity, light, pressure, rain } = this.props;

        const warningsPL = {
            light: 'Masz albinizm, a słońce świeci dziś mocno, więc przed wyjściem z domu nie zapomnij okularów i nakrycia głowy.',
        };

        let lightWarning = false;
        if (diseases.includes('albinism') && light > 500) {
            lightWarning = true;
        }
        return (
            <div className="disease-warning">
                <h5>Ostrzeżenie</h5>
                <ul>
                    {lightWarning && <li>{warningsPL.light}</li>}
                </ul>
            </div>
        );
    }
};