import React from 'react';
import { connect } from 'react-redux';
import { MenuBar } from '../_components';

class HelpPage extends React.Component {
    render() {
        const { user } = this.props;
        return (
            <div>
                <MenuBar username={user && user.firstName}/>
                <div className="col-md-10 col-md-offset-2">
                    <h1>Pomoc</h1>
                    <p>112</p>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { user } = state.authentication;
    return { user };
}
const connectedHelpPage = connect(mapStateToProps)(HelpPage);
export { connectedHelpPage as HelpPage };