# meteo-station-frontend

1. Run `npm install` to install project dependencies (only in first run).
2. If you haven't done it yet - run `meteo-station-backend`.
3. Run `npm start` to start React App, it will launch automatically in browser window.
